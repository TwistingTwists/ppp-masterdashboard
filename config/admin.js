module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'f640ea5ef3c73c9e0034f2e09130074e'),
  },
});
